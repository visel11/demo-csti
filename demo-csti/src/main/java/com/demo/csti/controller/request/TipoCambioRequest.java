package com.demo.csti.controller.request;

public class TipoCambioRequest {
	
	private String monedaOrigen;
	private String monedaDestion;
	private Double monto;
	
	public String getMonedaOrigen() {
		return monedaOrigen;
	}
	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}
	public String getMonedaDestion() {
		return monedaDestion;
	}
	public void setMonedaDestion(String monedaDestion) {
		this.monedaDestion = monedaDestion;
	}
	public Double getMonto() {
		return monto;
	}
	public void setMonto(Double monto) {
		this.monto = monto;
	}
}
