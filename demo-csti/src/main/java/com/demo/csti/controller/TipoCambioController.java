package com.demo.csti.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.csti.controller.request.TipoCambioRequest;
import com.demo.csti.dto.TipoCambioDto;
import com.demo.csti.service.TipoCambioService;

@RestController
public class TipoCambioController {

	@Autowired
	private TipoCambioService tipoCambioService;
	
	@PostMapping("/obtener-cambio")
	private TipoCambioDto obtenerCambio(@RequestBody TipoCambioRequest request) {
		return tipoCambioService.obtenerCambio(request);
	}
	
}
