package com.demo.csti.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.csti.entity.Student;
import com.demo.csti.repository.StudentRepository;

@Service
public class StudentService {

	@Autowired
	private StudentRepository studentRepository;

	// getting all student records
	public List<Student> getAllStudent() {
		return (List<Student>) studentRepository.findAll();
	}

	// getting a specific record
	public Student getStudentById(int id) {
		return studentRepository.findById(id).get();
	}

	public void saveOrUpdate(Student student) {
		studentRepository.save(student);
	}

	// deleting a specific record
	public void delete(int id) {
		studentRepository.deleteById(id);
	}
}
