package com.demo.csti.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.csti.controller.request.TipoCambioRequest;
import com.demo.csti.dto.TipoCambioDto;
import com.demo.csti.entity.TipoCambio;
import com.demo.csti.repository.TipoCambioRepository;

@Service
public class TipoCambioService {

	@Autowired
	private TipoCambioRepository tipoCambioRepository;
	
	public TipoCambioDto obtenerCambio (TipoCambioRequest request) {
		Optional<TipoCambio> optTipoCambio = tipoCambioRepository.findByMonedaOrigenAndMonedaDestino(request.getMonedaOrigen(), request.getMonedaDestion());
		return tipoCambioToDTO(request,optTipoCambio.get());
	} 
	
	private TipoCambioDto tipoCambioToDTO(TipoCambioRequest request, TipoCambio tipoCambio) {
		TipoCambioDto dto = new TipoCambioDto(); 
		dto.setMonto(request.getMonto());
		dto.setMontoCambio(request.getMonto() * tipoCambio.getTipoCambio());
		dto.setMonedaDestino(tipoCambio.getMonedaDestino());
		dto.setMonedaOrigen(tipoCambio.getMonedaOrigen());
		dto.setTipoCambio(tipoCambio.getTipoCambio());
		return dto;
	}
	
}
