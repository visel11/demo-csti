package com.demo.csti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoCstiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoCstiApplication.class, args);
	}

}
