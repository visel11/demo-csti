package com.demo.csti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TIPOCAMBIO")
public class TipoCambio {

	@Id
	@Column
	private int id;
	
	@Column(name = "MONEDAORIGEN")
	private String monedaOrigen;
	
	@Column(name = "MONEDADESTINO")
	private String monedaDestino;
	
	@Column(name = "TIPOCAMBIO")
	private Double tipoCambio;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMonedaOrigen() {
		return monedaOrigen;
	}

	public void setMonedaOrigen(String monedaOrigen) {
		this.monedaOrigen = monedaOrigen;
	}

	public String getMonedaDestino() {
		return monedaDestino;
	}

	public void setMonedaDestino(String monedaDestino) {
		this.monedaDestino = monedaDestino;
	}

	public Double getTipoCambio() {
		return tipoCambio;
	}

	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
}
