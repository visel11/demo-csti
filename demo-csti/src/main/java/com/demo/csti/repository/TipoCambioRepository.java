package com.demo.csti.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.demo.csti.entity.TipoCambio;

public interface TipoCambioRepository extends CrudRepository<TipoCambio, Integer>{	
	Optional<TipoCambio> findByMonedaOrigenAndMonedaDestino(String monedaOrigen, String monedaDestino);	
}
