package com.demo.csti.repository;

import org.springframework.data.repository.CrudRepository;

import com.demo.csti.entity.Student;

public interface StudentRepository extends CrudRepository<Student, Integer> {

}
